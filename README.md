# DeepMap
Last night I dreamt that somebody loved me.


Nicholas J. Tustison, Brian B. Avants, and James C. Gee.
Learning image-based spatial transformations via convolutional neural networks: a review,
_Magnetic Resonance Imaging_.  [(pubmed)](https://www.ncbi.nlm.nih.gov/pubmed/31200026)